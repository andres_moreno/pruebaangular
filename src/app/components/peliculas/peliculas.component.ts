import { Component, OnInit, DoCheck, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})
export class PeliculasComponent implements OnInit, DoCheck, OnDestroy {

  public titulo:string;

  constructor() { 
    console.log("CONSTRUCTOR LANZADO");

    this.titulo= "Aqui empieza mi Segundo Componente";
  }

  ngOnInit(): void {
    console.log("COMPONENTE INICIADO");
  }

  ngDoCheck(){
    console.log("DOCHECK LANZADO");
  }

  cambiarTitulo(){
    this.titulo= "El título ha sido cambiado";
  }

  ngOnDestroy(){
    console.log("EL COMPONENTE SE VA A ELIMINAR");
  }

}
